/*
 * Copyright (C) 2019 Siqura Ltd.
 *
 * Based on pico-8m.h
 * Author: Robert Suijker <r.suijker@siqura.com>
 *         Richard Hu <richard.hu@technexion.com>
 *         Tapani Utriainen <tapani@technexion.com>
 *         Po Cheng <po.cheng@technexion.com>
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#ifndef __SIQURA_8M_H
#define __SIQURA_8M_H

#include <linux/sizes.h>
#include <asm/arch/imx-regs.h>
#include "imx_env.h"

#ifdef CONFIG_SECURE_BOOT
#define CONFIG_CSF_SIZE			0x2000 /* 8K region */
#endif

#define CONFIG_SYS_BOOTM_LEN       0xA000000

#define CONFIG_SPL_TEXT_BASE		0x7E1000
#define CONFIG_SPL_MAX_SIZE		(148 * 1024)
#define CONFIG_SYS_MONITOR_LEN		(512 * 1024)
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_USE_SECTOR
#define CONFIG_SYS_MMCSD_RAW_MODE_U_BOOT_SECTOR	0x300
#define CONFIG_SYS_MMCSD_FS_BOOT_PARTITION	1

#ifdef CONFIG_SPL_BUILD
/*#define CONFIG_ENABLE_DDR_TRAINING_DEBUG*/
#define CONFIG_SPL_WATCHDOG_SUPPORT
#define CONFIG_SPL_DRIVERS_MISC_SUPPORT
#define CONFIG_SPL_POWER_SUPPORT
#define CONFIG_SPL_I2C_SUPPORT
#define CONFIG_SPL_LDSCRIPT		"arch/arm/cpu/armv8/u-boot-spl.lds"
#define CONFIG_SPL_STACK		0x187FF0
#define CONFIG_SPL_LIBCOMMON_SUPPORT
#define CONFIG_SPL_LIBGENERIC_SUPPORT
#define CONFIG_SPL_SERIAL_SUPPORT
#define CONFIG_SPL_GPIO_SUPPORT
#define CONFIG_SPL_MMC_SUPPORT
#define CONFIG_SPL_BSS_START_ADDR      0x00180000
#define CONFIG_SPL_BSS_MAX_SIZE        0x2000	/* 8 KB */
#define CONFIG_SYS_SPL_MALLOC_START    0x42200000
#define CONFIG_SYS_SPL_MALLOC_SIZE    0x80000	/* 512 KB */
#define CONFIG_SYS_SPL_PTE_RAM_BASE    0x41580000
#define CONFIG_SYS_ICACHE_OFF
#define CONFIG_SYS_DCACHE_OFF

#define CONFIG_MALLOC_F_ADDR		0x182000 /* malloc f used before GD_FLG_FULL_MALLOC_INIT set */

#define CONFIG_SPL_ABORT_ON_RAW_IMAGE /* For RAW image gives a error info not panic */

#undef CONFIG_DM_MMC
#undef CONFIG_DM_PMIC
#undef CONFIG_DM_PMIC_PFUZE100

#define CONFIG_SYS_I2C
#define CONFIG_SYS_I2C_MXC_I2C1		/* enable I2C bus 1 */
#define CONFIG_SYS_I2C_MXC_I2C2		/* enable I2C bus 2 */
#define CONFIG_SYS_I2C_MXC_I2C3		/* enable I2C bus 3 */

#define CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG

#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_PFUZE100
#define CONFIG_POWER_PFUZE100_I2C_ADDR 0x08
#endif

#define CONFIG_REMAKE_ELF

#define CONFIG_BOARD_EARLY_INIT_F
#define CONFIG_BOARD_POSTCLK_INIT
#define CONFIG_BOARD_LATE_INIT

/* Flat Device Tree Definitions */
#define CONFIG_OF_BOARD_SETUP

#undef CONFIG_CMD_IMLS

#undef CONFIG_CMD_CRC32
#undef CONFIG_BOOTM_NETBSD

/* ENET Config */
/* ENET1 */
#if defined(CONFIG_CMD_NET)
#define CONFIG_CMD_PING
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_MII
#define CONFIG_MII
#define CONFIG_ETHPRIME                 "FEC"

#define CONFIG_FEC_MXC
#define CONFIG_FEC_XCV_TYPE             RGMII
#define CONFIG_FEC_MXC_PHYADDR          1
#define FEC_QUIRK_ENET_MAC

#define CONFIG_PHY_GIGE
#define IMX_FEC_BASE			0x30BE0000

#define CONFIG_PHYLIB
#define CONFIG_PHY_ATHEROS
#endif

#define CMA_SIZE 256M
#define MMC_IMG_FUG_PART 2


/**
 * This script determines from a $fugstate (firmware upgrade state) which image to boot.
 * It is a collaboration between the boot loader and the firware.
 *
 * The initial state of $fugstate is FUG_NOT_PRESENT (0) (or variable not defined)
 *
 * When a new firmware is uploaded the old firmware sets the $fugstate to FUG_PRESENT (1).
 * The old firmware will then issue a restart and when the bootloader sees FUG_PRESENT, it
 * sets $fugstate to FUG_RUN_FIRST (2) to indicate a boot will be tried. Two things
 * can happen:
 *
 *   1. The new firmware boots normally and sets $fugstate to FUG_VALIDATED (3), indicating that
 *      the image booted successful. The firmware also will set $fit_current_image.
 *   2. The new firmware fails to boot, and a watchdog (hopefully) will reboot the device.
 *      The bootloader then sees FUG_RUN_FIRST and marks the sets $fugstate to FUG_INVALIDATED
 *      to indicate upgrade image failed.
 *
 * In case booting was successful the firmware sets the $fugstate to FUG_VALIDATED (3) and
 * $current_image to the loaded image passes as a kernel parameter by the bootloader.
 *
 * If booting failed due to watchdog triggering before the GCA application in the firmware fully
 * started the bootloader will set $fugstate to FUG_INVALIDATED.
 */
#define DETERMINE_BOOT_STATE \
	"loadfitimage=" \
		"env set fit_image none; " \
		"if env exists fugstate; then " \
			"if test $fugstate -eq 3; then " \
				"env set fit_image ${current_image}; " \
			"fi; " \
			"if test $fugstate -eq 2; then " \
				"env set fugstate 4; " \
					"run savefugstate; " \
				"fi; " \
				"if test $fugstate -eq 1; then " \
					"env set fit_image ${current_image}; " \
					"env set fugstate 2; " \
					"run savefugstate; " \
				"fi; " \
			"fi; " \
			"if test ${fit_image} != none && ext2load mmc ${mmcdev}:${mmcpart_fug} ${fit_addr} ${fit_image}; then " \
				"echo Running upgrade firmware image; " \
			"else echo Running factory firmware image; " \
			"env set fit_image base_image.fw; " \
			"fatload mmc ${mmcdev}:${mmcpart} ${fit_addr} ${fit_image}; " \
		"fi; " \
		"fdt addr $fit_addr; " \
		"fdt get value fit_version / version;\0" \

/* Initial environment variables */
#define CONFIG_EXTRA_ENV_SETTINGS		\
	DETERMINE_BOOT_STATE \
	"bootenv=fug.env\0" \
	"cma_size="__stringify(CMA_SIZE)"\0" \
	"console=ttymxc0,115200 earlycon=ec_imx6q,0x30860000,115200\0" \
	"displayautodetect=on\0" \
	"fit_addr=0x44000000\0" \
	"fit_high=0xffffffff\0" \
	"fitargs=setenv bootargs quiet console=${console} root=/dev/ram0 rootwait rw cma=${cma_size} fw_version=${fit_version} fw_image=${fit_image}\0" \
	"fitboard=houston\0" \
	"fitboot=run fitargs; bootm ${fit_addr}#config@${fitboard}\0" \
	"importbootenv=echo Importing environment from mmc ...; env import -t -r $loadaddr $filesize\0" \
	"loadbootenv=ext2load mmc ${mmcdev}:${mmcpart_fug} ${loadaddr} ${bootenv}\0" \
	"mmcautodetect=yes\0" \
	"mmcdev="__stringify(CONFIG_SYS_MMC_ENV_DEV)"\0" \
	"mmcpart=" __stringify(CONFIG_SYS_MMC_IMG_LOAD_PART) "\0" \
	"mmcpart_fug="__stringify(MMC_IMG_FUG_PART)"\0" \
	"netargs=setenv bootargs console=${console} root=/dev/nfs ip=dhcp nfsroot=${serverip}:${nfsroot},v3,tcp cma=${cma_size} fw_version=${fit_version} fw_image=${fit_image}\0" \
	"netboot=echo Booting from net ...; run netargs; echo ${bootargs}; run loadfitimage; bootm ${fit_addr}#config@${fitboard} - ${fdtaddr}\0" \
	"nfsroot=/nfsroot\0" \
	"savefugstate=env export ${loadaddr} fugstate current_image; ext4write mmc ${mmcdev}:${mmcpart_fug} ${loadaddr} /fug.env 0x1000\0" \
	"splashpos=m,m\0"

#define CONFIG_BOOTCOMMAND \
		"mmc dev ${mmcdev}; if mmc rescan; then " \
			"if run loadbootenv; then " \
				"echo Loaded environment from ${bootenv}; " \
				"run importbootenv; " \
				"fi; " \
				"if run loadfitimage; then " \
					"run fitboot; else " \
					"echo WARN: Cannot load kernel from boot media; "\
				"fi; " \
			"fi"


/* Link Definitions */
#define CONFIG_LOADADDR			0x40480000

#define CONFIG_SYS_LOAD_ADDR           CONFIG_LOADADDR

#define CONFIG_SYS_INIT_RAM_ADDR        0x40000000
#define CONFIG_SYS_INIT_RAM_SIZE        0x80000
#define CONFIG_SYS_INIT_SP_OFFSET \
        (CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
        (CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)

#define CONFIG_ENV_OVERWRITE
#define CONFIG_ENV_OFFSET               (64 * SZ_64K)
#define CONFIG_ENV_SIZE			0x1000
#define CONFIG_SYS_MMC_ENV_DEV		1   /* USDHC2 */
#define CONFIG_MMCROOT			"/dev/mmcblk1p2"  /* USDHC2 */

/* Size of malloc() pool */
#define CONFIG_SYS_MALLOC_LEN		((CONFIG_ENV_SIZE + (2*1024) + (16*1024)) * 1024)

#define CONFIG_SYS_SDRAM_BASE           0x40000000
#define PHYS_SDRAM                      0x40000000

#define PHYS_SDRAM_SIZE_1GB		SZ_1G /* 1GB DDR */
#define PHYS_SDRAM_SIZE_2GB		SZ_2G /* 2GB DDR */
#define PHYS_SDRAM_SIZE_3GB		0xC0000000 /* 3GB DDR */
#define PHYS_SDRAM_SIZE_4GB		0x100000000 /* 4GB DDR */

#define CONFIG_NR_DRAM_BANKS		1

#define CONFIG_SYS_MEMTEST_START    PHYS_SDRAM
#define CONFIG_SYS_MEMTEST_END      (CONFIG_SYS_MEMTEST_START + (PHYS_SDRAM_SIZE >> 1))

#define CONFIG_BAUDRATE			115200

#define CONFIG_MXC_UART
#define CONFIG_MXC_UART_BASE		UART1_BASE_ADDR

/* Monitor Command Prompt */
#undef CONFIG_SYS_PROMPT
#define CONFIG_SYS_PROMPT		"u-boot=> "
#define CONFIG_SYS_PROMPT_HUSH_PS2     "> "
#define CONFIG_SYS_CBSIZE              2048
#define CONFIG_SYS_MAXARGS             64
#define CONFIG_SYS_BARGSIZE CONFIG_SYS_CBSIZE
#define CONFIG_SYS_PBSIZE		(CONFIG_SYS_CBSIZE + \
					sizeof(CONFIG_SYS_PROMPT) + 16)

#define CONFIG_IMX_BOOTAUX

#define CONFIG_CMD_MMC
#define CONFIG_FSL_ESDHC
#define CONFIG_FSL_USDHC

#define CONFIG_SYS_FSL_USDHC_NUM	2
#define CONFIG_SYS_FSL_ESDHC_ADDR       0

#define CONFIG_SUPPORT_EMMC_BOOT	/* eMMC specific */
#define CONFIG_SYS_MMC_IMG_LOAD_PART	1
#define CONFIG_MXC_GPIO

#define CONFIG_MXC_OCOTP
#define CONFIG_CMD_FUSE

/* I2C Configs */
#define CONFIG_SYS_I2C_SPEED		  100000

/* USB configs */
#ifndef CONFIG_SPL_BUILD

#define CONFIG_CMD_USB
#define CONFIG_USB_STORAGE

#define CONFIG_CMD_USB_MASS_STORAGE
#define CONFIG_USB_GADGET_MASS_STORAGE
#define CONFIG_USB_FUNCTION_MASS_STORAGE

#define CONFIG_CMD_READ

#endif

#define CONFIG_SERIAL_TAG
#define CONFIG_FASTBOOT_USB_DEV 0


#define CONFIG_USB_MAX_CONTROLLER_COUNT         2

#define CONFIG_USBD_HS
#define CONFIG_USB_GADGET_VBUS_DRAW 2

#define CONFIG_OF_SYSTEM_SETUP

/* Watchdog support */
#define CONFIG_HW_WATCHDOG
#define CONFIG_WATCHDOG_TIMEOUT_MSECS 60000
#define CONFIG_IMX_WATCHDOG

/* Framebuffer */
#ifdef CONFIG_VIDEO
#define CONFIG_VIDEO_IMXDCSS
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_SPLASH_SCREEN
#define CONFIG_SPLASH_SCREEN_ALIGN
#define CONFIG_BMP_16BPP
#define CONFIG_VIDEO_LOGO
#define CONFIG_VIDEO_BMP_LOGO
#define CONFIG_IMX_VIDEO_SKIP
#endif

#endif